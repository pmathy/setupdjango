#!/usr/bin/env python3

#This script 
#   - gathers information from the user for the creation of required ENV files.
#   - creates the ENV files.
#   - runs docker-compose up to start the containers
#   - runs the database migration and the static files migration.
#   - creates a Django superuse for access to the admin site

from getpass import getpass
from time import sleep
from pathlib import Path
import os, subprocess, sys
from dryPackage import containerInfo

#Collect input for db.env file 
postgres_db = input("Please enter your preferred Database name.\n")
postgres_user = input("Please enter your Database Username.\n")
postgres_password = getpass("Please enter your Database Password. Please do not use stupid passwords.\n")
postgres_password_conf = getpass("Please confirm your Database Password.\n")

#Verify that the password and password confirmation match.
while postgres_password != postgres_password_conf:
    print("\nUnfortunately your passwords do not match. Please try again.\n")
    postgres_password = getpass("Please enter your Database Password.\n")
    postgres_password_conf = getpass("Please confirm your Database Password.\n")

#Create ./db directory and write collected information to db.env file
Path("./db").mkdir(parents=True, exist_ok=True)
db_env = open("./db/db.env", "w+")
db_env.write("POSTGRES_DB=" + postgres_db + "\n")
db_env.write("POSTGRES_USER=" + postgres_user + "\n")
db_env.write("POSTGRES_PASSWORD=" + postgres_password)
db_env.close()

#Collect missing input for .env.dev file
django_secret = getpass("Please enter your Django Secret Key. Please use a random string of at least 16 digits.\n")
django_secret_conf = getpass("Please confirm your Django Secret Key.\n")

#Verify that the key is sufficiently long.
while len(django_secret) < 16:
    print("\nUnfortunately your keys is too short. Please use at least 16 digits.\n")
    django_secret = getpass("Please enter your Django Secret Key.\n")
    django_secret_conf = getpass("Please confirm your Django Secret Key.\n")

#Verify that the key and key confirmation match.
while django_secret != django_secret_conf:
    print("\nUnfortunately your keys do not match. Please try again.\n")
    django_secret = getpass("Please enter your Django Secret Key.\n")
    django_secret_conf = getpass("Please confirm your Django Secret Key.\n")

#Write collected information to .env.dev file
db_env = open("./django/data/project_django/.env.dev", "w+")
db_env.write("DEBUG=1\n")
db_env.write("SECRET_KEY=" + django_secret + "\n")
db_env.write("DJANGO_ALLOWED_HOSTS=['.mathy-coerdt.com' 'localhost' '127.0.0.1' '[::1]']\n")
db_env.write("SQL_ENGINE=django.db.backends.postgresql\n")
db_env.write("SQL_DATABASE=" + postgres_db + "\n")
db_env.write("SQL_USER=" + postgres_user + "\n")
db_env.write("SQL_PASSWORD=" + postgres_password + "\n")
db_env.write("SQL_HOST=djangoapp_db1\n")
db_env.write("SQL_PORT=5432")
db_env.close()

#run Docker Compose Up command to start containers.
command_compose = "docker-compose up -d"
subprocess.call(command_compose, shell=True)

#get Webserver container status
container_status = subprocess.check_output("docker ps | grep djangoapp_web1 | awk '{print $10}'", shell=True)
container_status = container_status[0:-1].decode("utf-8")

#wait max 20 seconds for Webserver to come up
duration = 0
while container_status != "Up" and duration < 40:
    sleep(0.5)
    duration += 1

if duration == 40:
    print("Your Container Setup is not coming up, please verify your containers.")
    exit()

print("Waiting...")
sleep(30)

#get container id with function from DRY package
container_id = containerInfo.getContainerID('djangoapp_app1')

#concatenate container ID with command to run Python database migration
command_db = "docker exec " + container_id + " python manage.py migrate"

#execute concatenated command
subprocess.call(command_db, shell=True)

#concatenate container ID with command to copy static files to shared Volume for nginx
command_static = "docker exec " + container_id + " python manage.py collectstatic --no-input --clear"

#execute concatenated command
subprocess.call(command_static, shell=True)

print("\nThe following information is required to create a user for the Django admin site.")

#concatenate container ID with command to create Django superuser
command_su = "docker exec -it " + container_id + " python manage.py createsuperuser"

#execute concatenated command
subprocess.call(command_su, shell=True)

sys.exit(container_id)